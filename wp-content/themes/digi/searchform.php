<form action="/" method="get" class="search-input" aria-label="Search">
  <label class="screen-reader-text" for="search">Search:</label>
  <input type="text" name="s" id="search" value="<?php the_search_query(); ?>" placeholder="Search site..." aria-label="Search term" />
  <button type="submit" id="searchsubmit" aria-label="Search">
    <i class="fas fa-search"></i> <span>Search</span>
  </button>
</form>