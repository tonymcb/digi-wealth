<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  <?php if (get_the_content()) : ?>
    <div class="entry__content">
      <?php the_content(); ?>
    </div><!-- .entry-content -->
  <?php endif; ?>
<?php endwhile; endif; ?>
<?php get_footer(); ?>