<?php
defined( 'ABSPATH' ) or exit;
// Declare support for menus and thumbnails
if (function_exists('add_theme_support')) {
	add_theme_support('post-thumbnails');
  add_theme_support( 'menus' );
  // Add Gutenberg wide / full support
  add_theme_support( 'align-wide' );
  add_theme_support( 'custom-logo', array(
      'height'      => 100,
      'width'       => 400,
      'flex-height' => true,
      'flex-width'  => true,
      'header-text' => array( 'site-title', 'site-description' ),
  ) );
  add_theme_support( 'editor-styles' );
  add_editor_style( 'https://use.typekit.net/ikc7cml.css');
  add_editor_style( 'css/style-editor.css');
}
// Add support for excerpt in posts
function lp_add_excerpt_posts() {
  add_post_type_support( 'post', 'excerpt' );
}
add_action( 'init', 'lp_add_excerpt_posts' );
// Add support for SVGs
function add_file_types_to_uploads($file_types){
  $new_filetypes = array();
  $new_filetypes['svg'] = 'image/svg+xml';
  $file_types = array_merge($file_types, $new_filetypes );
  return $file_types;
  }
  add_filter('upload_mimes', 'add_file_types_to_uploads');
// Modify except to shorter and without brackets
function new_excerpt_length($length) {
    return 20;
}
add_filter('excerpt_length', 'new_excerpt_length');
function new_excerpt_more($more) {
  return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

// Register menus
function register_digi_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
      'footer-menu' => __( 'Footer Menu' ),
    )
  );
  // Remove emoji scripts
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
}
add_action( 'init', 'register_digi_menus' );

/*==================
  STYLES
==================*/
function add_styles() {
  // Add custom styles
  $digi_theme = wp_get_theme();
  $version = $digi_theme->get( 'Version' );
  wp_register_style('custom-style', tdir().'css/style.min.css', array(), $version);
  wp_register_style('slickcss', tdir().'lib/slick-1.8.1/slick/slick.css');
  if (!is_admin()) {
    wp_enqueue_style('slickcss');
    wp_enqueue_style('custom-style');
  }
}
add_action('wp_enqueue_scripts', 'add_styles');

add_theme_support( 'editor-styles' );
add_editor_style( tdir().'css/style-editor.css');

/*==================
  SCRIPTS
==================*/
// Updating jQuery
function modify_jquery() {
  if (!is_admin()) {
    wp_deregister_script('jquery');
    wp_register_script('jquery', tdir().'js/jquery-3.4.1.min.js', false, '3.4.1');
    wp_enqueue_script('jquery');
  }
}
add_action('wp_enqueue_scripts', 'modify_jquery');
function add_scripts() {
  // Add scripts
  $digi_theme = wp_get_theme();
  $version = $digi_theme->get( 'Version' );
  wp_register_script('customfunctions', tdir().'js/script.min.js', array(), $version);
  wp_register_script('flexibilitypf', tdir().'js/flexibility.js');
  wp_register_script('modernizr', tdir().'js/modernizr.min.js');
  wp_register_script('html5slider', tdir().'js/html5slider.js');
  wp_register_script('slickjs', tdir().'lib/slick-1.8.1/slick/slick.min.js');
  if (!is_admin()) {
    wp_enqueue_script('flexibilitypf');
    wp_enqueue_script('modernizr');
    wp_enqueue_script('slickjs');
    wp_enqueue_script('customfunctions');
    wp_enqueue_script('html5slider');
  }
}
add_action('wp_enqueue_scripts', 'add_scripts');


// ACF
if( function_exists('acf_add_local_field_group') ):

  acf_add_local_field_group(array(
    'key' => 'group_5b2779b94d15c',
    'title' => 'Contact details',
    'fields' => array(
      array(
        'key' => 'field_5b2779c975491',
        'label' => 'Email',
        'name' => 'global_email',
        'type' => 'email',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array(
          'width' => '',
          'class' => '',
          'id' => '',
        ),
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
      ),
      array(
        'key' => 'field_5b2779d775492',
        'label' => 'Phone',
        'name' => 'global_phone',
        'type' => 'text',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array(
          'width' => '',
          'class' => '',
          'id' => '',
        ),
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'maxlength' => '',
      ),
    ),
    'location' => array(
      array(
        array(
          'param' => 'options_page',
          'operator' => '==',
          'value' => 'acf-options-website-options',
        ),
      ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'seamless',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
  ));



endif;


if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
      'key' => 'group_5cbedd45e362d',
      'title' => 'Standard',
      'fields' => array(
        array(
          'key' => 'field_5cbedd49cf219',
          'label' => 'Subtitle',
          'name' => 'subtitle',
          'type' => 'text',
          'instructions' => '',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'maxlength' => '',
        ),
      ),
      'location' => array(
        array(
          array(
            'param' => 'post_type',
            'operator' => '==',
            'value' => 'page',
          ),
        ),
      ),
      'menu_order' => 0,
      'position' => 'acf_after_title',
      'style' => 'seamless',
      'label_placement' => 'top',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
      'active' => true,
      'description' => '',
    ));

endif;