<?php
defined( 'ABSPATH' ) or exit;
function img_qual() {
  return 90;
}
add_filter( 'jpeg_quality', 'img_qual' );
add_filter( 'image_send_to_editor', 'remove_thumbnail_dimensions', 10 );
function remove_thumbnail_dimensions( $html ) {
  $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html ); return $html;
}

add_image_size('full_banner', 1600, 800, true);
add_image_size('max', 1600, 1600);
add_image_size('card_image_retina', 1320, 680, true);
add_image_size('card_image', 660, 340, true);
add_image_size('square', 250, 250, true);
add_image_size('square_retina', 500, 500, true);