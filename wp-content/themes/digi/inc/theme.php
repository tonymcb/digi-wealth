<?php
defined( 'ABSPATH' ) or exit;
if( function_exists('acf_add_options_page') ) {
  acf_add_options_page(array(
    'page_title'  => __('Website Options'),
    'capability'    => 'edit_pages',
  ));
}

/**
 * Theme colour Setup
 *
 */
function digi_setup() {
	// Disable Custom Colors
  add_theme_support( 'disable-custom-colors' );
  // add_theme_support( '__experimental-disable-custom-gradients', true );
  // add_theme_support( 'disable-custom-gradients' );

	// Editor Color Palette
	add_theme_support( 'editor-color-palette', array(
		array(
			'name'  => __( 'Mint', 'digi' ),
			'slug'  => 'mint',
			'color'	=> '#99f3bb',
		),
		array(
			'name'  => __( 'Sky Blue', 'digi' ),
			'slug'  => 'skyblue',
			'color'	=> '#66dff0',
		),
		array(
			'name'  => __( 'Black', 'digi' ),
			'slug'  => 'black',
			'color' => '#010b19',
		),
		array(
			'name'  => __( 'White', 'digi' ),
			'slug'  => 'white',
			'color' => '#ffffff',
		),
		array(
			'name'  => __( 'Lavender', 'digi' ),
			'slug'  => 'lavender',
			'color' => '#9384f0',
		),
		array(
			'name'  => __( 'Midnight Blue', 'digi' ),
			'slug'  => 'midnightblue',
			'color' => '#001c43',
		),
  ) );
}
add_action( 'after_setup_theme', 'digi_setup' );


/*==================
  REGISTER ACF BLOCKS
==================*/
function register_acf_block_types() {
  // Register a block for the price calculator
  acf_register_block_type(array(
    'name'              => 'price-calculator',
    'title'             => __('Price calculator'),
    'description'       => __('An interactive widget to calculate price.'),
    'render_template'   => 'partials/blocks/price-calculator.php',
    'category'          => 'widgets',
		'mode' 							=> 'edit',
		'icon'              => 'money',
		'supports'					=> array(
			'align' => array( 'full', 'wide' ),
			'multiple' => false,
		),
    'keywords'          => array( 'price', 'interactive', 'calculator', 'block' ),
  ));
  // Contact details
  acf_register_block_type(array(
    'name'              => 'contact-details',
    'title'             => __('Contact details'),
    'description'       => __('Shows the site contact details.'),
    'render_template'   => 'partials/blocks/contact-details.php',
    'category'          => 'widgets',
		'mode' 							=> 'edit',
		'icon'              => 'phone',
		'supports'					=> array(
			'align' => array( 'full', 'wide' ),
			'multiple' => false,
		),
    'keywords'          => array( 'contact', 'email', 'phone', 'block' ),
  ));
  // Step
  acf_register_block_type(array(
    'name'              => 'step',
    'title'             => __('Step'),
    'description'       => __('Allows you to illustrate a step by step process.'),
    'render_template'   => 'partials/blocks/step.php',
    'category'          => 'layout',
		'mode' 							=> 'edit',
		'icon'              => 'editor-ol',
		'supports'					=> array(
			'align' => array( 'full', 'wide' ),
		),
    'keywords'          => array( 'step', 'process', 'block' ),
  ));
  // Tabbed section
  acf_register_block_type(array(
    'name'              => 'tabbed-section',
    'title'             => __('Tabbed Section'),
    'description'       => __('Tabbed content sections.'),
    'render_template'   => 'partials/blocks/tabbed-section.php',
    'category'          => 'layout',
		'mode' 							=> 'edit',
		'icon'              => 'editor-insertmore',
		'supports'					=> array(
			'align' => array( 'full', 'wide' ),
		),
    'keywords'          => array( 'tabbed', 'multiple', 'block' ),
  ));
  // Icon bullets
  acf_register_block_type(array(
    'name'              => 'icon-bullets',
    'title'             => __('Icon Bullets'),
    'description'       => __('Icon Bullets as lists.'),
    'render_template'   => 'partials/blocks/icon-bullets.php',
		'category'          => 'layout',
		'mode' 							=> 'edit',
		'icon'              => 'editor-ul',
		'supports'					=> array(
			'align' => array( 'full', 'wide' ),
		),
    'keywords'          => array( 'list', 'items', 'icons', 'block' ),
  ));
}

// Check if function exists and hook into setup.
if( function_exists('acf_register_block_type') ) {
  add_action('acf/init', 'register_acf_block_types');
}