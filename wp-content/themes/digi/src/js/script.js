jQuery(document).ready(function($) {

  /*
  * Price Calculator
  */
  if ($('.price-calculator').length) {
    // Get value and input elements
    var $adviser = $("#pc-adviser");
    var $adviserVal = $("#pc-adviser-val");
    var $investment = $("#pc-investment");
    var $investmentVal = $("#pc-investment-val");
    // Populate value elements with input values
    $adviserVal.html($adviser.val());
    $investmentVal.html(numberWithCommas($investment.val()));
    // Run calculation function
    pcCalc($adviserVal, $adviser, false);
    pcCalc($investmentVal, $investment, true);
    // On both range slider inputs, when modified,
    // populate value elements, then run calculation
    $adviser.on('input', function() {
      pcCalc($adviserVal, $adviser, false);
    });
    $investment.on('input', function() {
      pcCalc($investmentVal, $investment, true);
    });
  }

  // On menu toggle click (shows on small screens only)
  $('.navbar-toggler').on('click', function(e) {
    e.preventDefault();
    // Toggle active class
    $(this).toggleClass('is-active');
    var target = $(this).attr('data-target');
    // Toggle navigation
    $('#header-menu').slideToggle(100);
    $('#header-menu').toggleClass('is-open');
  });

  // mobile functionality for expanding menu
  $('#menu-main li').on('click', function(e) {
    // On click inside LI item in nav menu
    if ($('#menu-main.is-open').length) {
      // If mobile menu is open
      if ($(e.target).is('li')) {
        // If clicked target is LI and not A tag
        // Then stop propagation to get deepest clicked item
        e.stopPropagation();
        if ($( window ).width() < 977) {
          if ($(this).hasClass('toggled')) {
            // If already open
            $(this).find('> ul').slideUp();
            $(this).removeClass('toggled');
          } else if ($(this).hasClass('menu-item-has-children')) {
            // If element is mobile expand button
            $(this).find('> ul').slideDown();
            $(this).addClass('toggled');
          }
        }
      }
    }
  });

  // Script to tab through the dropdown menu
  $('#menu-main > li > a').each(function() {
    // if this has submenu
    if ($(this).next('ul')) {
      // Then on focus of this link
      $(this).on('focus', function() {
        // Add a focus class to top level li of this link to enable dropdown reveal styles
        $(this).parent().addClass('focus');
      });
      // Get submenu links and the last link
      var subMenu = $(this).next('ul');
      var subMenuLinks = subMenu.find('a');
      var lastLinkIndex = subMenuLinks.length - 1;
      var lastLink = $(subMenuLinks[lastLinkIndex]);

      // On unfocus of last link
      lastLink.on('blur', function() {
        // Remove the focus class from top level li element
        $(this).parent().parent().parent().removeClass('focus');
      });
    }
  });

  // Tabbed section
  if ($('.tabbed-section').length) {
    // Set up tab functions
    $('.tabbed-section .tabbed-section__tabs a').on('click', function(e) {
      e.preventDefault();
      var target = $(this).attr('href');
      $(this).parent().parent().find('.is-active').removeClass('is-active');
      $(this).addClass('is-active');
      $(target).parent().find('.is-active').hide().removeClass('is-active');
      $(target).show().addClass('is-active');
    });
  }

  // Detect any groups of pullquotes and wrap in slick
  if ($('.wp-block-pullquote.is-style-solid-color').length > 1) {
    $(':not(.wp-block-pullquote.is-style-solid-color) + .wp-block-pullquote.is-style-solid-color, * > .wp-block-pullquote.is-style-solid-color:first-of-type').
      each(function() {
        var wrapClass = ($(this).hasClass('alignfull')) ? ' alignfull' : '';
        $(this).
        nextUntil(':not(.wp-block-pullquote.is-style-solid-color)').
        addBack().
        wrapAll('<div class="testimonial-group'+wrapClass+'" />');
    });
    $('.testimonial-group').slick({
      dots: true,
      arrows: false,
      autoplay: true,
      autoplaySpeed: 6000,
      adaptiveHeight: true
    });
  }

});


// Dynamic margin and height according to content
function setTabbedAtts() {
  jQuery('.tabbed-section').each(function() {
    // Reset JS added attributes
    jQuery(this).css("marginBottom",0);
    jQuery(this).find('.tabbed-section__content').css("height","unset");
    // Loop through all content items and get highest
    var highest = 0;
    jQuery(this).find('.tabbed-section__content-item').each(function() {
      if (jQuery(this).outerHeight() > highest) {
        highest = jQuery(this).outerHeight();
      }
    });
    // Set height of content container
    jQuery(this).find('.tabbed-section__content').css('height', highest);
    // Set active content
    jQuery(this).find('.tabbed-section__content-item').hide();
    jQuery(this).find('.tabbed-section__content-item.is-active').show();

    // Get height of whole widget
    var widgetH = jQuery(this).height();

    // Create bottom margin for image
    if (jQuery(this).hasClass('tabbed-section--with-image')) {
      // Get image height
      var imgH = jQuery(this).find('.tabbed-section__image img').outerHeight();
      var thresh = (widgetH + 60);
      if (imgH > thresh) {
        var bottomM = imgH - widgetH;
        jQuery(this).css('marginBottom', bottomM);
      }
    }
  });
}
jQuery( window ).on('load', function() {
  setTabbedAtts();
  jQuery( window ).resize(function() {
    setTabbedAtts();
  });
});

function numberWithCommas(number) {
  var parts = number.toString().split(".");
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return parts.join(".");
}

function pcCalc($target, $input, commas) {
  // Set value element from input value
  if (commas) {
    $target.html(numberWithCommas($input.val()));
  } else {
    $target.html($input.val());
  }
  // Add percentage value as CSS left margin
  var thisPerc = (($input.val() - $input.attr('min')) / ($input.attr('max') - $input.attr('min')) * 100);
  $target.parent().parent().css({
    'margin-left': 'calc(' + thisPerc + '%)',
    'transform': 'translateX( -' + thisPerc + '%)' ,
  });
  // Get range slider values
  investmentAmount = jQuery('#pc-investment').val();
  adviserPerc = jQuery('#pc-adviser').val();
  // Calculate all values
  var investmentAmountPerc = investmentAmount / 100;
  var platPerc = jQuery('.price-calculator').attr('data-platform');
  var plat500Perc = jQuery('.price-calculator').attr('data-platform-500');
  var plat1mPerc = jQuery('.price-calculator').attr('data-platform-1m');
  var fundPerc = jQuery('.price-calculator').attr('data-funds');
  var breakpointA = 500000;
  var breakpointB = 1000000;
  // Split investment for platform fees
  if (investmentAmount >= breakpointB) {
    var platformA = parseFloat((breakpointA / 100) * platPerc).toFixed(2);
    var platformB = parseFloat(((breakpointB - breakpointA) / 100) * plat500Perc).toFixed(2);
    var platformC = (investmentAmount > breakpointB) ? parseFloat(((investmentAmount - breakpointB) / 100) * plat1mPerc).toFixed(2) : 0 ;
    var platform = (+platformA + +platformB + +platformC);
  } else if(investmentAmount >= breakpointA) {
    var platformA = parseFloat((breakpointA / 100) * platPerc).toFixed(2);
    var platformB = parseFloat(((investmentAmount - breakpointA) / 100) * plat500Perc).toFixed(2);
    var platform = (+platformA + +platformB);
  } else {
    var platform = parseFloat(investmentAmountPerc * platPerc).toFixed(2);
  }
  var funds = parseFloat(investmentAmountPerc * fundPerc).toFixed(2);
  var advise = parseFloat(investmentAmountPerc * adviserPerc).toFixed(2);
  var total = (+platform + +funds + +advise);
  // Insert calculations into elements
  jQuery('#pc-figure-platform').html(numberWithCommas(platform));
  jQuery('#pc-figure-funds').html(numberWithCommas(funds));
  jQuery('#pc-figure-advise').html(numberWithCommas(advise));
  jQuery('#pc-figure-total').html(numberWithCommas(parseFloat(total).toFixed(2)));
}