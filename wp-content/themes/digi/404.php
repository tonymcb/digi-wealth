<?php get_header(); ?>
<section class="fourohfour">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h2><?php echo __('Sorry, there was a problem finding that page.'); ?></h2>
				<p><?php echo __('Maybe try a search?'); ?></p>
				<?php get_search_form(); ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>