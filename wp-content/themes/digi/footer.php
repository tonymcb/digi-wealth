    </div>
    <footer>
      <div class="footer-logo">
        <a href="<?php echo site_url(); ?>" class="logo">
          <img src="<?php tp(); ?>/img/logo.svg" alt="<?php bloginfo('name'); ?>">
        </a>
      </div>
      <div class="contact-details">
        <a href="tel:<?php the_field('global_phone', 'option'); ?>" class="contact-details__link contact-details__link--tel"><?php the_field('global_phone', 'option'); ?></a>
        <a href="mailto:<?php the_field('global_email', 'option'); ?>" class="contact-details__link contact-details__link--email"><?php echo str_replace('@', '<span>@</span>', get_field('global_email', 'option')); ?></a>
      </div>
      <div id="copyright">
        &copy; Copyright Digital Wealth Systems Limited <?php echo esc_html( date_i18n( __( 'Y' ) ) ); ?>.
      </div>
      <div id="legal">
        <p>Digital Wealth Systems Limited is registered in England and Wales with company number 11619983 and has its registered office at 7 St. Petersgate, Stockport, England, SK1 1EB.</p>
          <p>Digi Wealth Platform is a trading name of P1 Investment Management Limited, which is authorised
          and regulated by the Financial Conduct Authority. registration 752005</p>
          <p>P1 Investment Management Limited is registered in England, number 09810560 Registered Office:
          Senate Court, Southernhay Gardens, Exeter, Devon, EX1 1NT</p>
          <p>Please note that telephone calls may be recorded in order to monitor the quality of customer service
          and for training purposes.</p>
      </div>
      <div id="footer-menu">
        <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
      </div>
    </footer>
  </main>
</div>
<?php wp_footer(); ?>
</body>
</html>
