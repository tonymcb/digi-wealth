module.exports = function(grunt) {
	
	const sass = require('node-sass');

	// Configure task(s)
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		uglify: {
			all: {
        files: grunt.file.expandMapping(['src/js/*.js', 'src/js/**/*.js'], 'js/', {
					rename: function(destBase, destPath) {
						return destBase+destPath.replace('src/js/','').replace('.js', '.min.js');
					}
        })
			},
		},
		sass: {
			dev: {
				options: {
					sourceMap: true,
					implementation: sass,
					outputStyle: 'compressed'
				},
				files: {
					'css/style.min.css' : 'src/scss/style.scss'
				}
			},
			build: {
				options: {
					sourceMap: false,
					implementation: sass,
					outputStyle: 'compressed'
				},
				files: {
					'css/style.min.css' : 'src/scss/style.scss'
				}
			}
		},
		imagemin: {
			files: {
        expand: true,
        cwd: 'src/img',
        src: ['**/*.{png,jpg,gif}'],
        dest: 'img/'
      }
    },
		watch : {
			js: {
				files: ['src/js/*.js','src/js/pages/*.js'],
				tasks: ['uglify:all']
			},
			css: {
				files: ['src/scss/**/*.scss'],
				tasks: ['sass:dev'],
			},
			images: {
				files: ['src/img/**/*.{png,jpg,gif}'],
				tasks: ['imagemin']
			}
		}
	});

	// Load the plugins
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-imagemin');

	// Register task(s)
	grunt.registerTask('default', ['uglify', 'sass:dev', 'imagemin']);
	grunt.registerTask('build', ['uglify', 'sass:build', 'imagemin']);

};