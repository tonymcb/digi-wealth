<?php
get_header(); ?>
<?php if (get_search_query()) : ?>
<section class="entry__content">
  <div class="search-results alignwide">
    <div class="row">
      <?php if ( have_posts() ) :  // results found?>
        <?php while ( have_posts() ) : the_post(); ?>
          <div class="col-12 col-xl-6">
            <?php get_template_part( 'partials/content', 'preview' ); ?>
          </div>
        <?php endwhile; ?>
        <?php
        global $wp_query, $wp_rewrite;

        // If more than one page, generate pagination links
        if ( $wp_query->max_num_pages > 1 ) : ?>
          <?php
          $paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
          $pagenum_link = html_entity_decode( get_pagenum_link() );
          $query_args   = array();
          $url_parts    = explode( '?', $pagenum_link );

          if ( isset( $url_parts[1] ) ) {
            wp_parse_str( $url_parts[1], $query_args );
          }

          $pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
          $pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

          $format  = $wp_rewrite->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
          $format .= $wp_rewrite->using_permalinks() ? user_trailingslashit( $wp_rewrite->pagination_base . '/%#%', 'paged' ) : '?paged=%#%';

          // Set up paginated links.
          $links = paginate_links( array(
            'base'     => $pagenum_link,
            'format'   => $format,
            'total'    => $wp_query->max_num_pages,
            'current'  => $paged,
            'mid_size' => 1,
            'add_args' => array_map( 'urlencode', $query_args ),
            'prev_text' => __( 'Previous', 'twentyfourteen' ),
            'next_text' => __( 'Next', 'twentyfourteen' ),
          ) );

          if ( $links ) :

          ?>
          <nav class="navigation pagination" role="navigation">
            <h1 class="screen-reader-text"><?php _e( 'Posts navigation', 'twentyfourteen' ); ?></h1>
            <div class="nav-links">
              <?php echo $links; ?>
            </div><!-- .pagination -->
          </nav><!-- .navigation -->
          <?php
          endif;
          ?>
        <?php endif; ?>
      <?php else :  // no results?>
        <article>
          <h5>No Results Found.</h5>
        </article>
      <?php endif; ?>
    </div>
  </div>
</section>
<?php else : ?>
<header class="entry__content">
  <h2 class="has-red-color"><em>Enter your search query:</em></h2>
  <?php get_search_form(true); ?>
</header>
<?php endif; ?>
<?php get_footer();