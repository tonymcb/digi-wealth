<?php get_header(); ?>
<section class="news news--archive">
  <div class="container">
    <div class="row">
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="col-12 col-xl-6">
          <?php get_template_part( 'partials/content', 'preview' ); ?>
        </div>
      <?php endwhile; endif; ?>
        <?php the_posts_pagination(); ?>
    </div>
  </div>
</section>
<?php get_footer(); ?>
