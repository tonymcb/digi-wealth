<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <title>
  <?php is_front_page() ? bloginfo('name') : wp_title(''); ?> |
  <?php is_front_page() ? bloginfo('description') : bloginfo('name'); ?></title>
  <meta charset="<?php bloginfo( 'charset' ); ?>" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0" />
  <?php wp_head(); ?>
  <link rel="apple-touch-icon" sizes="144x144" href="/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
  <link rel="manifest" href="/favicons/site.webmanifest">
  <link rel="mask-icon" href="/favicons/safari-pinned-tab.svg" color="#D62929">
  <link rel="shortcut icon" href="/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#d62929">
  <meta name="msapplication-config" content="/favicons/browserconfig.xml">
  <meta name="theme-color" content="#d62929">
  <link rel="stylesheet" href="https://use.typekit.net/ikc7cml.css">
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body <?php body_class(); ?>>
  <div class="page-wrap">
    <header <?php if (is_front_page()) : ?>class="front"<?php endif; ?>>
      <div class="container">
        <div class="top-bar">
          <div class="col-logo">
            <a href="<?php echo site_url(); ?>" class="logo">
              <img src="<?php tp(); ?>/img/logo.svg" alt="<?php bloginfo('name'); ?>">
            </a>
          </div>
          <?php
          // Check if header menu has navigation assigned
          if (has_nav_menu('header-menu')) : ?>
            <div class="col-toggler">
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header-menu" aria-controls="header-menu" aria-expanded="false" aria-label="Toggle navigation">
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
              </button>
            </div>
          <?php endif; ?>
          <?php
          $theme_locations = get_nav_menu_locations();
          // Check if header menu has navigation assigned
          if (has_nav_menu('header-menu')) : ?>
            <nav id="header-menu" role="navigation" class="col-nav">
              <?php
              $menu_items = wp_get_nav_menu_items($theme_locations['header-menu']);
              wp_nav_menu(array(
                'menu' => $theme_locations['header-menu'],
                'menu_class'=> 'navbar-nav',
                'container' => FALSE,
                'walker'    => new LP_Sublevel_Walker,
              ));
              ?>
              <a href="<?php the_field('cta_header_btn_link', 'option'); ?>" class="btn <?php
              if (get_field('cta_header_btn_link', 'option') == home_url( $wp->request ).'/') {
                echo 'is-active';
              }
              ?>"><?php the_field('cta_header_btn_text', 'option'); ?></a>
            </nav>
          <?php endif; ?>
        </div>
      </div>
      <?php get_template_part( 'partials/content', 'title' ); ?>
    </header>
    <main role="main" class="main-wrap">
      <div class="entry">
        <noscript>Javascript is required for the best experience of this website</noscript>