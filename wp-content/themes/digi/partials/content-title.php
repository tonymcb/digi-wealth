<?php
$id = get_the_id();
$blog = false;
if (is_home()) {
  $id = get_option( 'page_for_posts' );
  $blog = true;
}

if (!is_front_page()) : ?>
  <div class="entry__header">
    <div class="entry__page_title">
      <h1 class="entry__title">
        <?php
        if (is_category()) {
          $arch = get_the_archive_title();
          echo str_replace('Category: ', '', $arch);
        } else if (is_search()) {
          echo 'Search';
          if (get_search_query()) echo ' results for: &quot;' . get_search_query() . '&quot;';
        } else if (is_tag()) {
          $arch = get_the_archive_title();
          echo str_replace('Tag: ', 'News: ', $arch);
        } else if (is_month()) {
          $arch = get_the_archive_title();
          echo str_replace('Month: ', 'News: ', $arch);
        } else if (is_archive()) {
          $arch = get_the_archive_title();
          echo str_replace('Archives: ', '', $arch);
        } else {
          echo get_the_title($id);
        }
      ?></h1>
      <?php if (get_field('subtitle', $id)) : ?>
        <p class="entry__subtitle"><span><?php the_field('subtitle', $id); ?></span></p>
      <?php endif; ?>
    </div>
  </div>
<?php else : ?>
  <div class="entry__header entry__header--front">
    <div class="entry__page_title">
      <h1 class="entry__title"><?php
        // Get title string
        $title = get_the_title($id);
        // Split into array of words
        $title_arr = explode(' ', $title);
        // Highlight second word
        $title_arr[1] = '<em>'.$title_arr[1].'</em>';
        // Convert back to string
        $title = implode(' ', $title_arr);
        // Show new styled title
        echo $title;
      ?></h1>
      <?php if (get_field('subtitle', $id)) : ?>
        <p class="entry__subtitle entry__subtitle--large"><span><?php the_field('subtitle', $id); ?></span></p>
      <?php endif; ?>
      <?php if (get_field('introduction_text', $id)) : ?>
        <p class="entry__subtitle"><span><?php the_field('introduction_text', $id); ?></span></p>
      <?php endif; ?>
      <?php if (get_field('demo_button_link', $id) || get_field('find_out_more_button_link', $id)) : ?>
        <div class="entry__buttons">
          <?php if (get_field('find_out_more_button_link', $id)) : ?>
            <a class="btn btn--outline" href="<?php the_field('find_out_more_button_link', $id); ?>"><?php echo (get_field('find_out_more_button_text', $id)) ? get_field('find_out_more_button_text', $id) : 'Find out more' ; ?></a>
          <?php endif; ?>
          <?php if (get_field('demo_button_link', $id)) : ?>
            <a class="btn" href="<?php the_field('demo_button_link', $id); ?>"><?php echo (get_field('demo_button_text', $id)) ? get_field('demo_button_text', $id) : 'View a demo' ; ?></a>
          <?php endif; ?>
        </div>
      <?php endif; ?>
      <?php if (get_field('introduction_image', $id)) : ?>
        <?php $img = get_field('introduction_image', $id); ?>
        <img src="<?php echo $img['sizes']['max']; ?>" class="entry__image">
      <?php endif; ?>
    </div>
  </div>
<?php endif; ?>