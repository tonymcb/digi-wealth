<?php
/**
 * Tabbed section
 *
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'tabbed-section-' . $block['id'];
if( !empty($block['anchor']) ) {
  $id = $block['anchor'];
}
// Get variables
$tabs = get_field('tabs');
$image = get_field('tabbed_image');
$title = get_field('tabbed_title') ?: 'Title goes here';
$btn_link = get_field('more_button_link');
$btn_text = get_field('more_button_text') ?: 'Find out more';
// Create class attribute allowing for custom "className" and "align" values.
$className = 'tabbed-section';
if( !empty($block['className']) ) {
  $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
  $className .= ' align' . $block['align'];
}
if (!empty($image)) {
  $className .= ' tabbed-section--with-image';
}
?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
  <?php if (!empty($image)) : ?>
    <div class="tabbed-section__image">
      <img src="<?php echo $image['sizes']['max']; ?>" alt="<?php echo $image['alt']; ?>">
    </div>
  <?php endif; ?>
  <div class="tabbed-section__text">
    <h2 class="tabbed-section__title"><?php echo $title; ?></h2>
    <ul class="tabbed-section__tabs">
      <?php $i = 0; ?>
      <?php foreach($tabs as $tab) : ?>
        <?php $i++; ?>
        <li><a href="#<?php echo sanitize_title($tab['tab_title']); ?>"<?php if($i == 1) echo 'class="is-active"'; ?>><?php echo $tab['tab_title']; ?></a></li>
      <?php endforeach; ?>
    </ul>
    <div class="tabbed-section__content">
      <?php $i = 0; ?>
      <?php foreach($tabs as $tab) : ?>
        <?php $i++; ?>
        <div id="<?php echo sanitize_title($tab['tab_title']); ?>" class="tabbed-section__content-item<?php if($i == 1) echo ' is-active'; ?>">
          <?php echo $tab['tab_content']; ?>
        </div>
      <?php endforeach; ?>
    </div>
    <?php if (!empty($btn_link)) : ?>
      <a href="<?php echo $btn_link; ?>" class="btn btn--outline"><?php echo $btn_text; ?></a>
    <?php endif; ?>
  </div>
</div>