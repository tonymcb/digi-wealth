<?php
/**
 * Icon bullets
 *
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'icon-bullets-' . $block['id'];
if( !empty($block['anchor']) ) {
  $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className = 'icon-bullets';
if( !empty($block['className']) ) {
  $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
  $className .= ' align' . $block['align'];
}
// Get variables
$bullets = get_field('bullet');
$title = get_field('title');
$bg = get_field('background_colour');
$iconsize = get_field('icon_size')?: 'md';
$columns = get_field('columns')?: 1;
if( !empty($bg) && $bg != 'false') {
  $className .= ' has-background-color has-' . $bg . '-background-color';
}
if( !empty($columns) && $columns > 1 ) {
  $className .= ' has-columns has-' . $columns . '-columns';
}
?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
  <?php if (!empty($title)) : ?>
    <h2 class="icon-bullets__title"><?php echo $title; ?></h2>
  <?php endif; ?>
  <ul>
    <?php foreach ($bullets as $bullet) : ?>
      <li class="size-<?php echo $iconsize; ?>">
        <img class="icon-bullets__icon size-<?php echo $iconsize; ?>" src="<?php echo $bullet['ib_icon']; ?>" alt="<?php echo $bullet['ib_title']; ?>">
        <div class="icon-bullets__text">
          <h4 class="icon-bullets__bullet"><?php echo $bullet['ib_title']; ?></h4>
          <?php if (!empty($bullet['ib_description'])) : ?>
            <p class="icon-bullets__desc"><?php echo $bullet['ib_description']; ?></p>
          <?php endif; ?>
        </div>
      </li>
    <?php endforeach; ?>
  </ul>
</div>