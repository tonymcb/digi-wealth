<?php
/**
 * Price calculator
 *
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'price-calc-' . $block['id'];
if( !empty($block['anchor']) ) {
  $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className = 'price-calculator';
if( !empty($block['className']) ) {
  $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
  $className .= ' align' . $block['align'];
}

// Load values and assign defaults.
$min_adv_fee = get_field('minimum_adviser_fee') ?: '0.1';
$max_adv_fee = get_field('maximum_adviser_fee') ?: '10.0';
$def_adv_fee = get_field('default_adviser_fee') ?: '0.5';
$min_inv = get_field('minimum_investment') ?: '10000';
$max_inv = get_field('maximum_investment') ?: '2000000';
$def_inv = get_field('default_investment') ?: '200000';
$plat_perc = get_field('platform_perc') ? (get_field('platform_perc')) : '0.35';
$plat_perc_5 = get_field('platform_perc_500') ? (get_field('platform_perc_500')) : '0.25';
$plat_perc_1 = get_field('platform_perc_1m') ? (get_field('platform_perc_1m')) : '0.10';
$fund_perc = get_field('funds_perc') ? (get_field('funds_perc')) : '0.08';
?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>" data-platform="<?php echo $plat_perc; ?>" data-platform-500="<?php echo $plat_perc_5; ?>" data-platform-1m="<?php echo $plat_perc_1; ?>" data-funds="<?php echo $fund_perc; ?>">
  <div class="price-calculator__item">
    <label>Adviser Fee</label>
    <span class="price-calculator__value">
      <span class="price-calculator__value-wrap"><span id="pc-adviser-val"></span>%</span>
    </span>
    <input type="range" min="<?php echo $min_adv_fee; ?>" max="<?php echo $max_adv_fee; ?>" step="0.1" value="<?php echo $def_adv_fee; ?>" name="adviser" class="range-slider range-slider--adviser" id="pc-adviser">
  </div>
  <div class="price-calculator__item">
    <label>Investment Amount</label>
    <span class="price-calculator__value">
      <span class="price-calculator__value-wrap">£<span id="pc-investment-val"></span></span>
    </span>
    <input type="range" min="<?php echo $min_inv; ?>" max="<?php echo $max_inv; ?>" step="5000" value="<?php echo $def_inv; ?>" name="investment" class="range-slider range-slider--investment" id="pc-investment">
  </div>
  <div class="price-calculator__item price-calculator__item--figures">
    <div class="price-calculator__subtotals">
      <div class="price-calculator__figure">
        <label>Platform:</label> £<span id="pc-figure-platform"></span>
      </div>
      <div class="price-calculator__figure">
        <label>Funds:</label> £<span id="pc-figure-funds"></span>
      </div>
      <div class="price-calculator__figure">
        <label>Advise:</label> £<span id="pc-figure-advise"></span>
      </div>
    </div>
    <div class="price-calculator__figure price-calculator__total">
      <label>TOTAL:</label> £<span id="pc-figure-total"></span>
    </div>
  </div>
</div>