<?php
/**
 * Step
 *
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'step-' . $block['id'];
if( !empty($block['anchor']) ) {
  $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className = 'step';
if( !empty($block['className']) ) {
  $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
  $className .= ' align' . $block['align'];
}
// Get variables
$number = get_field('step_number') ?: '1';
$icon = get_field('step_icon') ?: 'check-all';
$title = get_field('step_title') ?: 'Title goes here';
$description = get_field('step_desc') ?: 'Description goes here';
?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
  <div class="step__icon step__icon--<?php echo $icon; ?>"></div>
  <div class="step__text">
    <p class="step__number">Step <span class="step__number__figure"><?php echo $number; ?></span></p>
    <h3 class="step__title"><?php echo $title; ?></h3>
    <?php echo $description; ?>
  </div>
</div>