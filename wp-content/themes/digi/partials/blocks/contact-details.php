<?php
/**
 * Contact details
 *
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'contact-details-' . $block['id'];
if( !empty($block['anchor']) ) {
  $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className = 'contact-details';
if( !empty($block['className']) ) {
  $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
  $className .= ' align' . $block['align'];
}
?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
  <a href="tel:<?php the_field('global_phone', 'option'); ?>" class="contact-details__link contact-details__link--tel"><?php the_field('global_phone', 'option'); ?></a>
  <a href="mailto:<?php the_field('global_email', 'option'); ?>" class="contact-details__link contact-details__link--email"><?php echo str_replace('@', '<span>@</span>', get_field('global_email', 'option')); ?></a>
</div>