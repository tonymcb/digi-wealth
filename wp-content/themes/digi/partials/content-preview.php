<a href="<?php the_permalink(); ?>" class="card card--preview">
  <div class="card__text <?php if (!has_post_thumbnail()) : ?>card__text--only<?php endif; ?>">
    <h3 class="card__title">
      <span class="card__title__text"><?php the_title(); ?></span>
    </h3>
    <?php the_excerpt(); ?>
    <span class="btn btn--block">Continue reading</span>
  </div>
  <?php if (has_post_thumbnail()) : ?>
    <div class="card__image">
      <?php $tid = get_post_thumbnail_id(); ?>
      <img src="<?php echo wp_get_attachment_image_url($tid, 'card_image' ); ?>" srcset="<?php echo wp_get_attachment_image_url($tid, 'card_image' ); ?> 1x, <?php echo wp_get_attachment_image_url($tid, 'card_image_retina' ); ?> 2x" alt="<?php the_title(); ?>">
    </div>
  <?php endif; ?>
</a>