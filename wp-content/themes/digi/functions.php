<?php
// Start the engine
require_once( get_template_directory() . '/inc/init.php' ); // Init, menu and theme files
require_once( get_template_directory() . '/inc/images.php' ); // image customisations
require_once( get_template_directory() . '/inc/theme.php' ); // add theme settings
require_once( get_template_directory() . '/inc/removals.php' ); // removing features

// Get theme directory path
function tdir () {return get_template_directory_uri().'/';}
function tp () {echo get_template_directory_uri().'/';}

// Retina images
function retina_img_srcs($filename='', $filetype='png') {
  $sw_theme = wp_get_theme();
  $version = $sw_theme->get( 'Version' );
	echo 'src="'.tdir().'img/'.$filename.'.'.$filetype.'?v='.$version.'" srcset="';
	echo tdir().'img/'.$filename.'.'.$filetype.'?v='.$version.' 1x, '.tdir().'img/'.$filename.'@2x.'.$filetype.'?v='.$version.' 2x"';
}

class LP_Sublevel_Walker extends Walker_Nav_Menu {
    function start_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class='sub-menu'>\n";
    }
    function end_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul>\n";
    }
}